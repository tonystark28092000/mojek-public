
const jwt = require("jsonwebtoken");
const User = require("../models/User");

const userAuth = async (req, res, next) => {
    try {
        const token = req.header("Authorization").replace("Bearer ", "");
        const decoded = jwt.verify(token, process.env.JWT_SALT);
        const user = await User.findOne({
            _id: decoded._id,
            "tokens.token": token,
        });
        if (!user) {
            console.log("No user found");
            throw new Error();
        }
        console.log(user.email);
        req.token = token;
        req.user = user;

        next();
    } catch (e) {
        res.status(401).send({ error: "Please Authenticate" });
    }
};

module.exports = userAuth;
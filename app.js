const express = require('express')
const cors = require("cors");
const bodyParser = require("body-parser");
const { mongoose } = require("./db/mongoose");

const app = express()

app.use(bodyParser.urlencoded({ extended: false, limit: "50mb" }));
app.use(bodyParser.json({ limit: "50mb" }));
app.use(cors());

const port = process.env.PORT || 3000

const userRoutes = require("./routes/user");
const accountRoutes = require("./routes/account");

app.use("/api/user", userRoutes);
app.use("/api/account", accountRoutes);

app.listen(port, (err) => {
    if (err) {
        return console.log("Error running the app");
    }
    console.log(`App is running on port ${port}`);
})
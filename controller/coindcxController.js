const crypto = require("crypto");
const axios = require("axios");

const getTransactions = async (key, secret) => {
	const baseurl = "https://api.coindcx.com";
	const timeStamp = Math.floor(Date.now());

	const body = {
		limit: 50,
		timestamp: timeStamp,
	};

	//setting the credentials for coin dcx api
	const payload = new Buffer(JSON.stringify(body)).toString();
	const signature = crypto
		.createHmac("sha256", secret)
		.update(payload)
		.digest("hex");

	const options = {
		url: baseurl + "/exchange/v1/orders/trade_history",
		headers: {
			"X-AUTH-APIKEY": key,
			"X-AUTH-SIGNATURE": signature,
		},
		json: true,
		body: body,
	};

	//calling the api
	const reponse = await axios.post(
		options.url,
		body,
		{
			headers: options.headers,
		}
	);

	return reponse;
};

const getBalances = async (key, secret) => {
	const baseurl = "https://api.coindcx.com";
	const timeStamp = Math.floor(Date.now());

	const body = {
		limit: 50,
		timestamp: timeStamp,
	};

	//setting credentials for coindcx api
	const payload = new Buffer(JSON.stringify(body)).toString();
	const signature = crypto
		.createHmac("sha256", secret)
		.update(payload)
		.digest("hex");

	const options = {
		url: baseurl + "/exchange/v1/users/balances",
		headers: {
			"X-AUTH-APIKEY": key,
			"X-AUTH-SIGNATURE": signature,
		},
		json: true,
		body: body,
	};

	//calling the api
	const reponse = await axios.post(
		options.url,
		body,
		{
			headers: options.headers,
		}
	);

	return reponse;
};

const getUserInfo = async (key, secret) => {
	const baseurl = "https://api.coindcx.com";
	const timeStamp = Math.floor(Date.now());

	const body = {
		timestamp: timeStamp,
	};

	//settiung the credentials for the coin dcx api
	const payload = new Buffer(JSON.stringify(body)).toString();
	const signature = crypto.createHmac("sha256", secret)
		.update(payload)
		.digest("hex");

	const options = {
		url: baseurl + "/exchange/v1/users/info",
		headers: {
			"X-AUTH-APIKEY": key,
			"X-AUTH-SIGNATURE": signature,
		},
		json: true,
		body: body,
	};

	//calling the api
    const reponse = await axios.post(
		options.url,
		body,
		{
			headers: options.headers,
		}
	);

	return reponse;
};

module.exports = {
	getTransactions,
	getBalances,
	getUserInfo,
};

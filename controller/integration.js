const Integration = require("../models/Integrations");
const Balances = require("../models/Balances");
const moment = require("moment");

const addIntegration = (integrationData) => {
    return new Promise(async (resolve, reject) => {
        try {
            const integration = new Integration(integrationData);
            await integration.save();
            resolve(integration);
        } catch (error) {
            reject(error)
        }
    })
}

//
// Self is TRUE for custom partner integrated accounts - ex: zerodha, BUC
//
const removeintegration = (accountID, self = false, userID) => {
    return new Promise(async (resolve, reject) => {
        let match = {
            user: userID
        };
        if (self) {
            match._id = accountID;
        } else {
            match.accountID = accountID;
        }
        try {
            await Integration.findOneAndDelete(match);
            resolve();
        } catch (error) {
            reject(error);
        }
    })
}

const isAvailableIntegration = (match) => {
    return new Promise((resolve, reject) => {
        Integration.findOne(match).exec((err, integration) => {
            if (err) {
                return reject(err)
            };
            if (!integration) {
                return resolve({ status: false })
            } else {
                return resolve({ status: true })
            }
        })
    })
}

const getAccoundIDFromProvider = (userID, providerID) => {
    return new Promise((resolve, reject) => {
        Integration.findOne({
            user: userID,
            "provider.id": providerID
        }).exec((err, integration) => {
            if (err) {
                return reject(err)
            }
            resolve(integration);
        })
    })
}

const getPortFolio = (userID) => {
    return new Promise(async (resolve, reject) => {
        const today = moment().startOf('day')
        const last7thDay = moment().subtract(6, "days");
        const sevenDaysBeforelast7thDay = last7thDay.subtract(7, "days").startOf('day');
        const oneDayBeforeLast7thDay = moment().subtract(7, "days").startOf('day');

        let portFolioTotal = await Balances.aggregate([
            {
                $match: {
                    user: userID,
                    createdAt: {
                        $gte: today.toDate(),
                        $lte: moment(today).endOf('day').toDate()
                    },
                }
            },
            {
                $group: {
                    _id: null,
                    total: { $sum: '$fiat_value' }
                },
            },
        ])

        let groupByAssets = await Balances.aggregate([
            {
                $match: {
                    user: userID,
                    createdAt: {
                        $gte: today.toDate(),
                        $lte: moment(today).endOf('day').toDate()
                    },
                }
            },
            {
                $group: {
                    _id: '$type',
                    total: { $sum: '$fiat_value' },
                    assets: {
                        $push: {
                            provider: '$provider.name',
                            displayName: '$provider.display_name',
                            providerID: '$provider.id',
                            asset: '$name',
                            total: { $sum: '$fiat_value' }
                        }
                    }
                },
            },
            // {
            //     $lookup: {
            //         from: "providers",
            //         localField: "assets.providerID",
            //         foreignField: "_id",
            //         as: "providersData"
            //     }
            // },
            // {
            //     $group: {
            //         _id: '$_id',
            //         integrations: {
            //             $push: {
            //                 channel: '$integrations.channel',
            //                 total: { $sum: '$integrations.total' }
            //             }
            //         }
            //     }
            // }
        ])

        for (let i = 0; i < groupByAssets.length; i++) {
            const groupByAsset = groupByAssets[i];
            const total = groupByAsset.total;

            for (let j = 0; j < groupByAsset.assets.length; j++) {
                const asset = groupByAsset.assets[j];
                const assetTotal = asset.total;

                let percentage = (assetTotal / total) * 100;
                percentage = percentage.toFixed(2);
                groupByAsset.assets[j].percentageConsumption = percentage;
            }
            groupByAssets[i].total = groupByAssets[i].total.toFixed(2);
        }

        let chartDataLast7days = await Balances.aggregate([
            {
                $match: {
                    user: userID,
                    createdAt: {
                        $gte: moment().subtract(6, "days").startOf('day').toDate(),
                        $lte: moment(today).endOf('day').toDate()
                    },
                }
            },
            {
                $group: {
                    _id: "$balanceDate",
                    total: { $sum: '$fiat_value' }
                }
            },
            {
                $sort: {
                    "_id": 1
                }
            }
        ])

        let chartDataBeforeLast7days = await Balances.aggregate([
            {
                $match: {
                    user: userID,
                    createdAt: {
                        $gte: sevenDaysBeforelast7thDay,
                        $lte: oneDayBeforeLast7thDay
                    },
                }
            },
            {
                $group: {
                    _id: "$balanceDate",
                    total: { $sum: '$fiat_value' }
                }
            },
            {
                $sort: {
                    "_id": 1
                }
            }
        ])

        portFolioTotal = portFolioTotal.length && portFolioTotal[0] ? portFolioTotal[0].total : 0;
        portFolioTotal = portFolioTotal.toFixed(2);

        resolve({
            portFolioTotal, assets: groupByAssets, chartData: {
                latest: {
                    // Convert the dates using moment in the frontend
                    dates: {
                        from: moment().subtract(6, "days").startOf('day'),
                        to: moment(today).endOf('day').toDate()
                    },
                    data: chartDataLast7days
                },
                comparisonChart: {
                    // Convert the dates using moment in the frontend
                    dates: {
                        from: sevenDaysBeforelast7thDay,
                        to: oneDayBeforeLast7thDay
                    },
                    data: chartDataBeforeLast7days
                }
            }
        });
    })
}

const getIntegration = (match) => {
    return new Promise((resolve, reject) => {
        Integration.findOne(match).exec((err, integration) => {
            if (err) {
                return reject(err)
            }
            if (!integration) {
                return reject({ error: true, message: "Invalid account" })
            }
            resolve(integration)
        })
    })
}

module.exports = {
    addIntegration,
    removeintegration,
    isAvailableIntegration,
    getAccoundIDFromProvider,
    getPortFolio,
    getIntegration
}
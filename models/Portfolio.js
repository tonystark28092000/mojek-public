const mongoose = require("mongoose");
var Float = require('mongoose-float').loadType(mongoose);

var portfolioSchema = new mongoose.Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "user",
        required: true
    },
    platform: {
        type: String,
        enum: ["BUC", "ZERODHA"],
        required: true
    },
    portfolioValue: {
        type: Float,
        required: true
    }
});

portfolioSchema.set("timestamps", true);

const Portfolio = mongoose.model("portfolio", portfolioSchema);

module.exports = Portfolio;
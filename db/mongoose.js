const mongoose = require("mongoose");
mongoose.Promise = global.Promise;
mongoose.connect(process.env.MONGO_URL, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true,
}, (err) => {
    if (err) {
        console.log(err);
        throw err
    }
    console.log("Connected to db");
});
module.exports = { mongoose };